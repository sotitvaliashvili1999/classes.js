class Character {
    constructor(name, roll ){
        this.name = name;
        this.roll = roll;
    }
    damage(){
        console.log(`${this.name} has a ${this.roll === "mage" || this.roll === "support"? "Magic Damage":"Attack Damage"}`)
    }
    armor(armor){
        console.log(`${this.name} has ${armor} armor `);
    }
}
class Mage extends Character{
    constructor(name, magicPowers = false){
        super(name, "mage");
        this.magicPowers = magicPowers;
    }
    flying(){
        console.log(`${this.name} is ${this.roll} and can flying`);
    }
}
class Support extends Mage{
    constructor(name, magicPowers, heals){
        super(name, "support", magicPowers);
        this.heals = heals;
    }
    healing(){
        console.log(`${this.name} can heal of ${heals} unit`);
    }
}
class Adc extends Character{
    constructor(name, attackDamages){
        super(name, "adc");
        this.attackDamages = attackDamages;
    }
    damageRange(){
        console.log(`${this.name} is ${this.roll} and  has ${this.attackDamages > 30? "LONG" : "SHORT"} attack damage `)
    }
}
